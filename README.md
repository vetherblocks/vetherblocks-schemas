# VEtheRBlocks-schemas

JSON schemas and samples used in VEtheRBlocks games. Work in progress.

## Structure

- `task.schema.json`: The JSON schema that defines a task and packet structures.
  - Todo: Split out into component parts.
- `samples/`
  - `brickdefs.json`: A sample definition of brick colours, HSV values, and acceptable ranges when detecting them.
  - `arp.task.json`: A sample task with a simple network and ARP query/response.

## Contributors

VEtheRBlocks has had the following contributors:

- Steve Kerrison, Senior Lecturer, Cybersecurity, James Cook University Singapore Campus
- Jusak Jusak, Senior Lecturer, Internet of Things, James Cook University Singapore Campus
- Bryant Soputan, Research Assistant, James Cook University Singapore Campus
- Licong Huang, Research Assistant, James Cook University Singapore Campus
